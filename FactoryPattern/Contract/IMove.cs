﻿namespace FactoryPattern.Contract
{
    public interface IMove
    {
        string MakeMove();
    }
}
