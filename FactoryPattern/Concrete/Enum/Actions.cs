﻿namespace FactoryPattern.Concrete.Enum
{
    public enum Actions
    {
        Up,
        Down,
        Right,
        Left,
        Combo
    }
}
