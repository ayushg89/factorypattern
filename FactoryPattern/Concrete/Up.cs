﻿using FactoryPattern.Contract;

namespace FactoryPattern.Concrete
{
    public class Up : IMove
    {
        private readonly string moveName = "Basic Up Move";

        public virtual string MakeMove()
        {
            // Implement Up logic
            return moveName;
        }
    }

}
