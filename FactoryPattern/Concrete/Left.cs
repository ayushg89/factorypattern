﻿using FactoryPattern.Contract;

namespace FactoryPattern.Concrete
{
    public class Left : IMove
    {
        private readonly string moveName = "Basic Left Move";

        public virtual string MakeMove()
        {
            // Implement MoveLeft logic
            return moveName;
        }
    }
}
