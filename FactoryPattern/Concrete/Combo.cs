﻿using FactoryPattern.Builder;
using FactoryPattern.Contract;
using System.Collections.Generic;
using FactoryPattern.Factory;

namespace FactoryPattern.Concrete
{
    public class Combo : IMove
    {
        private List<IMove> _listMoves;
        readonly FactoryPattern.Builder.Builder _builder;
        public Combo(MoveFactory obj)
        {
            _builder = new ComboBuilder(obj);
            _builder.CombinationOfMove();
        }

        public string MakeMove()
        {
            _listMoves = _builder.GetMoves();
            foreach (IMove move in _listMoves)
            {
                move.MakeMove();
            }
            return _builder.MoveName();
        }
    }
}
