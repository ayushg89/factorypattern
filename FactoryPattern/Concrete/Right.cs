﻿using System;
using System.Collections.Generic;
using System.Text;
using FactoryPattern.Contract;

namespace FactoryPattern.Concrete
{
    public class Right : IMove
    {
        private readonly string moveName = "Basic Right Move";

        public virtual string MakeMove()
        {
            // Implement MoveRight logic
            return moveName;
        }
    }
}
