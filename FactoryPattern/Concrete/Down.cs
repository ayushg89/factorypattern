﻿using FactoryPattern.Contract;

namespace FactoryPattern.Concrete
{
    public class Down : IMove
    {
        private readonly string moveName = "Basic Down Move";

        public virtual string MakeMove()
        {
            // Implement MoveDown logic
            return moveName;
        }
    }
}
