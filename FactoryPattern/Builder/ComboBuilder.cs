﻿using FactoryPattern.Concrete.Enum;
using FactoryPattern.Contract;
using System.Collections.Generic;
using FactoryPattern.Factory;

namespace FactoryPattern.Builder
{
    public class ComboBuilder : FactoryPattern.Builder.Builder
    {
        private readonly MoveFactory _moveFactory;
        private readonly List<IMove> _listMoves;
        string moveName = "Up Up Down Down Combo Move";
        public ComboBuilder(MoveFactory obj)
        {
            _moveFactory = obj;
            _listMoves = new List<IMove>();
        }

        public override void CombinationOfMove()
        {
            _listMoves.Add(_moveFactory.Get(Actions.Up));
            _listMoves.Add(_moveFactory.Get(Actions.Up));
            _listMoves.Add(_moveFactory.Get(Actions.Down));
            _listMoves.Add(_moveFactory.Get(Actions.Down));
        }

        public override List<IMove> GetMoves()
        {
            return _listMoves;
        }

        public override string MoveName()
        {
            return moveName;
        }
    }
}
