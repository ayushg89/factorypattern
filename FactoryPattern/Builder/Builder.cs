﻿using System.Collections.Generic;
using FactoryPattern.Contract;

namespace FactoryPattern.Builder
{
    public abstract class Builder
    {
        public abstract void CombinationOfMove();
        public abstract List<IMove> GetMoves();
        public abstract string MoveName();
    }
}
