﻿using System;
using FactoryPattern.Concrete.Enum;
using FactoryPattern.Contract;
using FactoryPattern.Factory;

namespace FactoryPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            MoveFactory objectFactory = new MoveFactory();

            IMove move = objectFactory.Get(Actions.Up);
            Console.WriteLine(move.MakeMove());

            move = objectFactory.Get(Actions.Down);
            Console.WriteLine(move.MakeMove());

            move = objectFactory.Get(Actions.Left);
            Console.WriteLine(move.MakeMove());

            move = objectFactory.Get(Actions.Right);
            Console.WriteLine(move.MakeMove());

            move = objectFactory.Get(Actions.Combo);
            Console.WriteLine(move.MakeMove());

            Console.ReadLine();
        }
    }
}
