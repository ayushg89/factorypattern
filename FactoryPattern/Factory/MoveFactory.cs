﻿using System;
using FactoryPattern.Concrete;
using FactoryPattern.Concrete.Enum;
using FactoryPattern.Contract;

namespace FactoryPattern.Factory
{
    public class MoveFactory
    {
        private Actions _moveType;
        public IMove Get(Actions type)
        {
            _moveType = type;
            if (_moveType == Actions.Combo)
            {
                return new Combo(this);
            }
            else
            {
                return (IMove)Activator.CreateInstance(Type.GetType($"FactoryPattern.Concrete.{_moveType}") ?? throw new InvalidOperationException());
            }
        }
    }
}
